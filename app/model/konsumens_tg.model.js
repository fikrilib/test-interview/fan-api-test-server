module.exports = (sequelize, Sequelize) => {
	const Konsumens_tg = sequelize.define('t_tg_konsumen', {
	  nama: {
			type: Sequelize.STRING
	  },
	  nominal: {
			type: Sequelize.INTEGER
	  },
	  kode_trans: {
		  type: Sequelize.STRING
	  },
	  status: {
		  type: Sequelize.STRING
	  }
	});
	
	return Konsumens_tg;
}