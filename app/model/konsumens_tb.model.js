module.exports = (sequelize, Sequelize) => {
	const Konsumens_tb = sequelize.define('t_tb_konsumen', {
	  nama: {
			type: Sequelize.STRING
	  },
	  nominal: {
			type: Sequelize.INTEGER
	  },
	  kode_trans: {
		  type: Sequelize.STRING
	  },
	  status: {
		  type: Sequelize.STRING
	  }
	});
	
	return Konsumens_tb;
}